CREATE DATABASE IMDBTEST
USE IMDBTEST
CREATE TABLE Producers (Id int identity primary key,Name varchar(100) ,Company varchar(100),CompanyEstDate date)
CREATE TABLE Actors (Id int identity primary key,Name varchar(100) ,Gender varchar(10),DOB date)
CREATE TABLE Movies (Id int identity primary key,Name varchar(100) ,Language varchar(10),ProducerId int references Producers(Id),Profit decimal(18,6))
CREATE TABLE MovieActor (MovieId int references Movies(id),Actors int references Actors(id))

--DROP TABLE MovieActor
--DROP TABLE Actors
--DROP TABLE Movies
--DROP TABLE Producers

INSERT INTO Producers VALUES ('Arjun','Fox','05/14/1998')
INSERT INTO Producers VALUES ('Arun','Bull','09/11/2004')
INSERT INTO Producers VALUES ('Tom','Hanks','11/03/1987')
INSERT INTO Producers VALUES ('Zeshan','Chillie','11/14/1996')

INSERT INTO Actors VALUES ('Mila Kunis','Female','11/14/1986')
INSERT INTO Actors VALUES ('Robert DeNiro','Male','07/10/1957')
INSERT INTO Actors VALUES ('George Michael','Male','11/23/1978')
INSERT INTO Actors VALUES ('Mike Scott','Male','08/06/1969')
INSERT INTO Actors VALUES ('Pam Halpert','Female','09/26/1996')
INSERT INTO Actors VALUES ('PRAKASH','male','09/26/1950')


INSERT INTO Movies VALUES ('Rocky','English',1,10000)
INSERT INTO Movies VALUES ('Rocky','Hindi',2,3000)
INSERT INTO Movies VALUES ('Terminal','English',1,300000)
INSERT INTO Movies VALUES ('Rambo','English',3,93000)

INSERT INTO MovieActor VALUES (1,1)
INSERT INTO MovieActor VALUES (1,3)
INSERT INTO MovieActor VALUES (1,4)
INSERT INTO MovieActor VALUES (2,2)
INSERT INTO MovieActor VALUES (3,4)
INSERT INTO MovieActor VALUES (3,5)
INSERT INTO MovieActor VALUES (3,2)
INSERT INTO MovieActor VALUES (3,1)
INSERT INTO MovieActor VALUES (4,1)
INSERT INTO MovieActor VALUES (4,2)
INSERT INTO MovieActor VALUES (1,5)
INSERT INTO MovieActor VALUES (2,5)
INSERT INTO MovieActor VALUES (3,5)
INSERT INTO MovieActor VALUES (4,5)
INSERT INTO MovieActor VALUES (4,6)


SELECT * FROM MovieActor
SELECT * FROM Actors

--Update Profit of all the movies by +1000 where producer name contains 'run'
UPDATE M
SET M.Profit = M.Profit + 1000
FROM Movies M INNER JOIN Producers P
ON M.ProducerId = P.Id 
WHERE P.Name LIKE '%run%'

--List down actors details which have acted in movies where producer name ends with 'n'
SELECT A.Name, A.DOB, A.Gender
FROM Actors A 
INNER JOIN MovieActor MV ON A.Id = MV.Actors
INNER JOIN Movies M ON MV.MovieId = M.Id
INNER JOIN Producers P ON P.Id = M.ProducerId
WHERE P.Name LIKE '%n'

--Find the avg age of male and female actors that were part of a movie called 'Terminal'  --Should return two rows 
SELECT A.Gender, AVG(DATEDIFF(year,A.DOB,GETDATE()))
FROM Actors A
INNER JOIN MovieActor MV ON MV.Actors = A.Id
INNER JOIN Movies M ON M.Id = MV.MovieId
WHERE M.Name = 'Terminal'
GROUP BY A.Gender

--Find the third oldest female actor
SELECT * FROM
	(SELECT TOP 3 A.*
	FROM Actors A
	WHERE A.Gender = 'Female'
	ORDER BY DOB) 
AS A1
WHERE A1.Id NOT IN 
	(SELECT TOP 2 A.Id
	FROM Actors A
	WHERE A.Gender = 'Female'
	ORDER BY DOB 
	)



--List down top 3 profitable movies 
SELECT TOP 3 * 
FROM Movies
ORDER BY Profit DESC


--List down the oldest actor and Movie Name for each movie
--SELECT A.Name AS '[Actor Name]', M.Name AS '[Movie Name]'

--SELECT DISTINCT A1.Name, T1.Id
--FROM Actors A1 
--INNER JOIN (
--	SELECT MIN(A.DOB) AS DOB,M.Id
--	FROM Actors A
--	INNER JOIN MovieActor MA ON MA.Actors = A.Id
--	INNER JOIN Movies M ON M.Id = MA.MovieId 
--	GROUP BY M.Id) 
--AS T1
--ON T1.DOB = A1.DOB



SELECT A1.Name, T1.MovieId, Mov.Name
FROM MovieActor MA1 
INNER JOIN (
	SELECT MIN(A.DOB) AS DOB,M.Id AS MovieId
	FROM Actors A
	INNER JOIN MovieActor MA ON MA.Actors = A.Id
	INNER JOIN Movies M ON M.Id = MA.MovieId 
	GROUP BY M.Id) 
AS T1
ON T1.MovieId = MA1.MovieId 
INNER JOIN Actors A1
ON MA1.Actors = A1.Id
INNER JOIN Movies AS Mov
ON Mov.Id = T1.MovieId
WHERE MA1.MovieId = T1.MovieId AND T1.DOB = A1.DOB


--Find duplicate movies having the same name and their count
SELECT M.Name , COUNT(M.id)
FROM Movies M
GROUP BY M.Name


--List down all the producers and +the movie name(even if they dont have a movie)
SELECT Producers.Name,Movies.Name
FROM Producers 
LEFT OUTER JOIN Movies ON Movies.ProducerId = Producers.Id


--After all the queries are done:
--Normalise it (If not normalised)
CREATE TABLE Companies(Id int identity primary key, Company varchar(100),CompanyEstDate date)
CREATE TABLE Producers (Id int identity primary key,Name varchar(100) ,CompanyId int references Companies(Id))
CREATE TABLE Actors (Id int identity primary key,Name varchar(100) ,Gender varchar(10),DOB date)
CREATE TABLE Movies (Id int identity primary key,Name varchar(100) ,Language varchar(10),ProducerId int references Producers(Id),Profit decimal(18,6))
CREATE TABLE MovieActor (MovieId int references Movies(id),Actors int references Actors(id))