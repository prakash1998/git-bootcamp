﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDBApp.Domains;
using IMDBApp.Repositories;
namespace IMDBApp
{
    public class Service
    {
        private ActorRepository _actorRepository = new ActorRepository();
        private ProducerRepository _producerRepository = new ProducerRepository();
        private MovieRepository _movieRepository = new MovieRepository();

        public void AddActor(Actor actor)
        {
            _actorRepository.AddActor(actor);
        }

        public Actor GetActor(string name)
        {
            return _actorRepository.GetActor(name);
        }

        public List<Actor> GetActors()
        {
            return _actorRepository.Actors;
        }
        public void AddProducer(Producer producer) 
        {
            _producerRepository.AddProducer(producer);
        }
        public Producer GetProducer(string name)
        {
            return _producerRepository.GetProducer(name);
        }

        public List<Producer> GetProducers()
        {
            return _producerRepository.Producers;
        }



        public void AddMovie(Movie movie)
        {
            _movieRepository.AddMovie(movie);
        }

        public List<Movie> GetMovies()
        {
            return _movieRepository.ListMovies();
        }

        public Movie GetMovie(string name)
        {
            return _movieRepository.GetMovie(name);
        }

        public void DeleteMovie(string name)
        {
            _movieRepository.DeleteMovie(name);
        }

    }
}
