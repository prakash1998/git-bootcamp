﻿using System;
using IMDBApp.Repositories;
using System.Linq;
using IMDBApp.Domains;
using System.Collections.Generic;

namespace IMDBApp
{
    class Program
    {
        static Service _services;
        public static void AddActor()
        {
            Actor actor = new Actor();
            Console.WriteLine("enter the name of the Actor : ");
            do
            {
                Console.WriteLine("the string can't be empty : ");
                actor.Name = Console.ReadLine();
            } while (actor.Name.Length == 0);
            Console.WriteLine("\n DOB(dd/mm/yyyy) : ");
            actor.DOB = Console.ReadLine();

            _services.AddActor(actor);
        }

        public static Producer AddProducer()
        {
            Producer producer = new Producer();
            Console.WriteLine("enter the name of the Producer : ");
            do
            {
                Console.WriteLine("the string can't be empty : ");
                producer.Name = Console.ReadLine();
            } while (producer.Name.Length == 0);


            Console.WriteLine("\n DOB(dd/mm/yyyy) : ");
            producer.DOB = Console.ReadLine();

            _services.AddProducer(producer);
            return producer;
        }
        public static void Main()
        {
            _services = new Service();
            while (true)
            {
                Console.WriteLine(" 1. List Movies");
                Console.WriteLine(" 2. Add Movie");
                Console.WriteLine(" 3. Add Actor");
                Console.WriteLine(" 4. Add Producer");
                Console.WriteLine(" 5. Delete Movie");
                Console.WriteLine(" 6. Exit");
                //int userInput = Convert.ToInt32(Console.ReadLine()); 
                if (!int.TryParse(Console.ReadLine(), out int userInput) || userInput < 1 || userInput > 6)
                {
                    Console.WriteLine("please enter a valid input");
                    continue;
                }

                switch (userInput)
                {

                    case 1:
                        Console.WriteLine(" \n --- List of movies --- \n");
                        List<Movie> movies;

                        movies = _services.GetMovies();
                        movies.ForEach(movie => Console.WriteLine(movie));

                        /*movies.Select(movie => { Console.WriteLine(movie.ToString()); return movie; });*/
                        break;
                    case 2:

                        Movie movie = new Movie();
                        Console.WriteLine(" \n --- Adding a movie --- \n");
                        Console.WriteLine("Name of the movie : ");
                        do
                        {
                            Console.WriteLine("the string can't be empty : ");
                            movie.Name = Console.ReadLine();
                        } while (movie.Name.Length == 0);

                        Console.WriteLine("\n");
                        Console.WriteLine("Year of release : ");
                        //this value is set to default
                        string yearOfRelease;
                        do
                        {
                            Console.WriteLine("yearOfRelease must be a number : ");
                            yearOfRelease = Console.ReadLine();
                        } while (!yearOfRelease.All(char.IsDigit));
                        movie.Year = Convert.ToInt32(yearOfRelease);
                        
                 
                        //movie.Year = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("\n");
                        Console.WriteLine("plot of the movie : ");
                        movie.Plot = Console.ReadLine();
                        Console.WriteLine("\n");

                        Console.WriteLine("select the producer among the following");
                        int selectedProducer = 0;
                        while (selectedProducer != 1)
                        {
                            List<Producer> producers = _services.GetProducers();

                            if (producers.Count == 0)
                            {
                                Console.WriteLine("it seems producer list is empty \n");
                                Console.WriteLine("please add the producer below");
                                movie.MovieProducer = AddProducer();

                                break;
                            }

                            int producerId = 0;
                            producers.ForEach(pro => Console.WriteLine(producerId + 1 + " . " + pro.Name + "\n"));


                            //int selectedId = -1;

                            //selectedId = Convert.ToInt32(Console.ReadLine());
                            if (int.TryParse(Console.ReadLine(), out int selectedId) && selectedId > 0 && selectedId < producers.Count)
                            {
                                selectedProducer = 1;
                                movie.MovieProducer = producers[selectedId - 1];
                            }
                            else
                            {
                                Console.WriteLine("select a valid id from the producers list");
                            }
                        }

                        int selectedActors = 0;
                        while (selectedActors != 1)
                        {
                            List<Actor> actors = _services.GetActors();

                            if (actors.Count == 0)
                            {
                                Console.WriteLine("it seems the actors list is emptry \n");
                                Console.WriteLine("Enter the number of actors you want to add \n");
                                //int actorsCount = Convert.ToInt32( Console.ReadLine());
                                string actorsCounter;
                                do
                                {
                                    Console.WriteLine("the number can't be empty : ");
                                    actorsCounter = Console.ReadLine();
                                } while (!actorsCounter.All(char.IsDigit));
                                int actorsCount = Convert.ToInt32(actorsCounter);
                                while (actorsCount != 0)
                                {
                                    AddActor();
                                    actorsCount--;
                                }


                            }


                            Console.WriteLine("select the actors among the following with space seperated");
                            int actorId = 1;
                            actors.ForEach(actor => Console.WriteLine(actorId++ + " . " + actor.Name + "\n"));
                            string[] ids = Console.ReadLine().Split(" ");

                            movie.Actors = new List<Actor>();
                            foreach (var i in ids)
                            {
                                if (int.TryParse(i, out int k) && k > 0 && k <= actors.Count)
                                {

                                    movie.Actors.Add(actors[k - 1]);
                                    selectedActors = 1;

                                }
                                else
                                {
                                    Console.WriteLine("please provide correct input ");
                                    //System.Environment.Exit(1);
                                }
                            }
                        }

                        _services.AddMovie(movie);

                        break;
                    case 3:
                        AddActor();
                        break;
                    case 4:
                        AddProducer();
                        break;
                    case 5:
                        int j = 1;
                        List<Movie> lmovies = _services.GetMovies();
                        Console.WriteLine("select a movie to delete : \n");
                        lmovies.ForEach(movie => Console.WriteLine(j++ + " " + movie.Name + "\n"));
                        //imov = Convert.ToInt32(Console.ReadLine());
                        if (int.TryParse(Console.ReadLine(), out int imov) && imov > 0 && imov < lmovies.Count)
                        {
                            _services.DeleteMovie(lmovies[imov - 1].Name);
                        }
                        else
                        {
                            Console.WriteLine("enter a valid input next time");
                        }
                        break;
                    case 6:

                        System.Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("option is invalid");
                        break;

                }

            }

        }
    }
}

