﻿Feature: SpecFlowFeature1
	I need a system to maintain all the 
	actors, producers and movies

@addMovie
Scenario: Add Movie
	Given I have a movie with title "Ironman"
	And I have a year of relase as "2019"
	And I have a plot as "the techie saving the world"
	And I have a actor named "tony stark , andrew"
	And I have a producer named "prakash"
	When I add the movi e to list like
	Then the MovieList should contain the movie with
	| title   | yearOfRelease | plot                        | producer | actors             |
	| Ironman | 2019          | the techie saving the world | prakash  | tony stark, andrew |
	

@listMovie
Scenario: List Movies
	Given I have a list of movies
	When I fetch all movies
	Then the list should be like
	| title   | yearOfRelease | plot                        | actors             | producer |
	| Ironman | 2019          | the techie saving the world | tony stark, andrew | prakash  |
