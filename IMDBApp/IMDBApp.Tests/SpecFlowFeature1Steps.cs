﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using IMDBApp.Domains;
using IMDBApp;
using System.Linq;
using System.Collections.Generic;
using Xunit;
namespace IMDBApp.Tests
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        Movie movie = new Movie();
        //Actor actor = new Actor("","");
        //producer producer = new Producer("", "");
        private Service _iMDB_Service = new Service();

        private readonly ScenarioContext scenarioContext;

        public SpecFlowFeature1Steps(ScenarioContext context)
        {
            scenarioContext = context;
        }
        [Given(@"I have a movie with title ""(.*)""")]
        public void GivenIHaveAMovieWithTitle(string title)
        {
            movie.Name = title;

        }
        
        [Given(@"I have a year of relase as ""(.*)""")]
        public void GivenIHaveAYearOfRelaseAs(int releaseYear)
        {
            movie.Year = releaseYear;
        }
        
        [Given(@"I have a plot as ""(.*)""")]
        public void GivenIHaveAPlotAs(string plot)
        {
            movie.Plot = plot;
        }
        
        [Given(@"I have a actor named ""(.*)""")]
        public void GivenIHaveAActorNamed(string actors)
        {
            string[] actorValues = actors.Split(',').Select(actorValue => actorValue.Trim()).ToArray();
            List<Actor> mactors = new List<Actor>();
            foreach(string actor in actorValues)
            {
                mactors.Add(_iMDB_Service.GetActor(actor)); 
            }
            movie.Actors = mactors;

        }
        
        [Given(@"I have a producer named ""(.*)""")]
        public void GivenIHaveAProducerNamed(string name)
        {
            movie.MovieProducer = _iMDB_Service.GetProducer(name);
        }
        [When(@"I add the movi e to list like")]
        public void WhenIAddTheMoviEToListLike()
        {
            _iMDB_Service.AddMovie(movie);
        }



        [Then(@"the MovieList should contain the movie with")]
        public void ThenTheMovieListShouldContainTheMovieWith(Table table)
        {
            Movie generate_movie = new Movie();
            generate_movie.Name = table.Rows[0]["title"];
            generate_movie.Year = Convert.ToInt32(table.Rows[0]["yearOfRelease"]);
            generate_movie.Plot = table.Rows[0]["plot"];
            Producer generated_producer = new Producer();
            generate_movie.MovieProducer = generated_producer;
            generate_movie.MovieProducer.Name = table.Rows[0]["producer"];
            String actors = table.Rows[0]["actors"];
            string[] actorValues = actors.Split(',').Select(actorValue => actorValue.Trim()).ToArray();
            List<Actor> mactors = new List<Actor>();
            foreach (string actor in actorValues)
            {
                mactors.Add(_iMDB_Service.GetActor(actor));
            }
            generate_movie.Actors = mactors;

            Assert.Equal(generate_movie, movie);


        }


        [Given(@"I have a list of movies")]
        public static void GivenIHaveAListOfMovies()
        {

        }
        
        [When(@"I fetch all movies")]
        public void WhenIFetchAllMovies()
        {
            var movies = _iMDB_Service.GetMovies();
            scenarioContext.Add("movies", movies);
        }
        
        [Then(@"the list should be like")]
        public void ThenTheListShouldBeLike(Table table)
        {
            var movies = (List<Movie>)scenarioContext["movies"];

            Assert.Equal(movies.Count, table.Rows.Count);
            Movie generate_movie = new Movie();
            Producer generated_producer = new Producer();
            List<Actor> mactors = new List<Actor>();
            Movie currentMovie;
            int noOfRows = table.Rows.Count;
            for(int i = 0; i < noOfRows; i++)
            {
                generate_movie.Name = table.Rows[i]["title"];
                currentMovie = _iMDB_Service.GetMovie(generate_movie.Name);
                generate_movie.Year = Convert.ToInt32(table.Rows[i]["yearOfRelease"]);
                generate_movie.Plot = table.Rows[i]["plot"];
                
                generate_movie.MovieProducer = generated_producer;
                generate_movie.MovieProducer.Name = table.Rows[i]["producer"];
                String actors = table.Rows[i]["actors"];
                string[] actorValues = actors.Split(',').Select(actorValue => actorValue.Trim()).ToArray();
                mactors.Clear();
                foreach (string actor in actorValues)
                {
                    mactors.Add(_iMDB_Service.GetActor(actor));
                }
                generate_movie.Actors = mactors;

                Assert.Equal(currentMovie,generate_movie);

            }

        }

        [BeforeScenario("addMovie")]
        public void AddTestData1()
        {
            _iMDB_Service.GetMovies().Clear();
            _iMDB_Service.GetActors().Clear();
            _iMDB_Service.GetProducers().Clear();
            Actor tony = new Actor("tony stark", "13/12/1998");
            Actor andrew = new Actor("andrew", "13/12/1998");
            Producer prakash = new Producer("prakash", "13/12/1998");
            _iMDB_Service.AddActor(tony);
            _iMDB_Service.AddActor(andrew);
            _iMDB_Service.AddProducer(prakash);


        }

        [BeforeScenario("listMovie")]
        public void AddTestData()
        {
            _iMDB_Service.GetMovies().Clear();
            _iMDB_Service.GetActors().Clear();
            _iMDB_Service.GetProducers().Clear();
            Actor tony = new Actor("tony stark", "13/12/1998");
            Actor andrew = new Actor("andrew", "13/12/1998");
            Producer prakash = new Producer("prakash", "13/12/1998");
            List<Actor> actors = new List<Actor>();
            actors.Add(tony);
            actors.Add(andrew);
            _iMDB_Service.AddActor(andrew);
            _iMDB_Service.AddActor(tony);
            
            _iMDB_Service.AddProducer(prakash);
            Movie ironman = new Movie("Ironman", 2019, "the techie saving the world", actors, prakash);
            _iMDB_Service.AddMovie(ironman);
        }
    }
}
