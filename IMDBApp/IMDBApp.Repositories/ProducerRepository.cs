﻿
using System.Collections.Generic;

using IMDBApp.Domains;

namespace IMDBApp.Repositories
{
    public class ProducerRepository
    {

        public List<Producer> Producers { get; }

        public ProducerRepository()
        {
            Producers = new List<Producer>();
        }
        public void AddProducer(Producer producer)
        {
            Producers.Add(producer);
        }
        public Producer GetProducer(string name)
        {
            return Producers.Find(producer=>producer.Name == name);

        }
    }
}
