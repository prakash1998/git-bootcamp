﻿using System;
using IMDBApp.Domains;
using System.Collections.Generic;
namespace IMDBApp.Repositories
{
    public class MovieRepository
    {
        private List<Movie> _movies = new List<Movie>();

        public void AddMovie(Movie movie)
        {
            _movies.Add(movie);
        }
        public List<Movie> ListMovies()
        {
            return _movies;
        }

        public Movie GetMovie(string name)
        {
            return _movies.Find(movie => movie.Name == name);
        }

        public void DeleteMovie(string name)
        {
            _movies.Remove(_movies.Find(movie => movie.Name == name));
        }
    }
}
