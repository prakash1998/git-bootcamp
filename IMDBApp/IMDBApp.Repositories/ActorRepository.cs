﻿
using System.Collections.Generic;

using IMDBApp.Domains;
namespace IMDBApp.Repositories
{
    public class ActorRepository
    {
        public List<Actor> Actors { get; }
        public ActorRepository()
        {
            Actors = new List<Actor>();
        }
        

        public void AddActor(Actor actor)
        {
            Actors.Add(actor);
        }

        public Actor GetActor(string name)
        {
            return Actors.Find(actor => actor.Name == name);
        }


    }
}
