﻿
using System.Collections.Generic;

using System.Linq;
namespace IMDBApp.Domains
{
    public class Movie
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; }
        public Producer MovieProducer { get; set; }
        public Movie(string Name, int Year, string Plot, List<Actor> Actors, Producer Producer)
        {
            this.Name = Name;
            this.Year = Year;
            this.Plot = Plot;
            this.Actors = Actors;
            this.MovieProducer = Producer;
        }
        public Movie()
        {
        }

        public override string ToString()
        {

            string ActorsString = string.Join(" ,", this.Actors.Select(actor => new string(actor.Name)));
            return "Name : " + Name + "\n" + "Actors : " + ActorsString + "\n" + "Year of Release : " + Year + "\n" + "Plot :" + Plot + "\n" + "Producer :" + MovieProducer.Name + "\n" + "-----------" +"\n";
          
        }
        public override bool Equals(object obj)
        {
            var movie = obj as Movie;
            if(movie == null)
            {
                return false;
            }
            if (this.Name != movie.Name) 
            {
                return false;
            }
            if(this.MovieProducer.Name != movie.MovieProducer.Name)
            {
                return false;
            }
            if(this.Plot != movie.Plot)
            {
                return false;
            }
            if(this.Actors.Count != movie.Actors.Count)
            {
                return false;
            }
            int compared = 0;
            for(int i = 0; i< this.Actors.Count; i++)
            {
                for(int j = 0; j < movie.Actors.Count; j++)
                {
                    if(this.Actors[j].Name == movie.Actors[i].Name)
                    {
                        compared++;
                    }
                }
            }
            if(compared != this.Actors.Count)
            {
                return false;
            }

            return true ;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }


}
