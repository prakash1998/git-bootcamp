﻿using System;
using TechTalk.SpecFlow;
using Xunit;
using System.Collections.Generic;
namespace Oops.Test
{
    [Binding]
    public class AttendanceSteps
    {
        private Services.PersonService _service = new Services.PersonService();
        Models.Person person;
        DateTime currentDate;
        
        [Given(@"I have a student with Id as ""(.*)""")]
        public void GivenIHaveAStudentWithIdAs(int id)
        {
            person = _service.GetPerson(id);
            
        }
        
        [When(@"When I mark the student as present for ""(.*)""")]
        public void WhenWhenIMarkTheStudentAsPresentFor(string date)
        {
            Models.Attendance attendance = new Models.Attendance();
            attendance.Date = DateTime.Parse(date);
            currentDate = attendance.Date;
            attendance.Present = true;
            _service.MarkAttendance(person, attendance);
            //person.Attendance.Add(attendance);
        }
        
        [Then(@"the attendance for the student for the day should be ""(.*)""")]
        public void ThenTheAttendanceForTheStudentForTheDayShouldBe(string boolvalue)
        {
            bool value = Convert.ToBoolean(boolvalue);
            Assert.Equal(person.Attendance.Find(att => att.Date == currentDate).Present,value);
        }

        [BeforeScenario("mark")]
        public void AddTestDate()
        {
            Models.Person person = new Models.Student();
            person.Id = 1251515;
            person.Attendance = new List<Models.Attendance>();
            _service.AddPerson(person);
            
        }

    }

    

}
