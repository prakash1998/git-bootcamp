﻿using System;
using System.Collections.Generic;
using System.Text;
using Oops;
namespace Oops.Services
{
    public class PersonService : IPersonService
    {
        
        private Repositories.PersonRepository _personRepo = new Repositories.PersonRepository();
        public void MarkAttendance( Models.Person person, Models.Attendance attendance)
        {
            _personRepo.GetPerson(person.Id).Attendance.Add(attendance);
        }
        public List<Models.Attendance> GetAttendance(int id)
        {
            return _personRepo.GetAttendances(id);
        }

        public Models.Person GetPerson(int id)
        {
            return _personRepo.GetPerson(id);
        }

        public void AddPerson(Models.Person person)
        {
            _personRepo.Add(person);
        }

        public void AddTestDate()
        {
            _personRepo.AddTestData();
        }

        public List<Models.Person> ListPersons()
        {
            return _personRepo.ListPersons();
        }
    }
}
