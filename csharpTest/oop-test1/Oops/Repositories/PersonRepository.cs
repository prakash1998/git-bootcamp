﻿using Oops.Models;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Oops.Repositories
{
    public class PersonRepository
    {
        private List<Person> _people = new List<Person>();

        public void AddTestData()
        {
            _people.Clear();
            Models.Attendance attendance = new Attendance();
            attendance.Date = DateTime.Parse("09/01/2020");
            attendance.Present = true;
            attendance.Date = DateTime.Parse("10/01/2020");
            attendance.Present = false;

            Models.Student student = new Models.Student();
            student.Id = 100;
            Marks marks = new Marks();
            student.Marks = marks;
            student.Marks.Subject = "Programming";
            student.Marks.TotalMarks = 100;
            student.Attendance = new List<Models.Attendance>();
            student.Attendance.Add(attendance);
            _people.Add(student);

            Models.Attendance attendance1 = new Attendance();
            attendance1.Date = DateTime.Parse("09/01/2020");
            attendance1.Present = true;
            attendance1.Date = DateTime.Parse("10/01/2020");
            attendance1.Present = false;

            Models.Student student1 = new Models.Student();
            Marks marks1 = new Marks();
            student1.Marks = marks1;
            student1.Id = 101;
            student1.Marks.Subject = "Programming";
            student1.Marks.TotalMarks = 100;
            student1.Attendance = new List<Models.Attendance>();
            student1.Attendance.Add(attendance1);
            _people.Add(student1);


            Models.Attendance attendance3 = new Attendance();
            attendance3.Date = DateTime.Parse("09/01/2020");
            attendance3.Present = true;
            attendance3.Date = DateTime.Parse("10/01/2020");
            attendance3.Present = false;

           
            Models.Student student2 = new Models.Student();
            student2.Id = 102;
            Marks marks2 = new Marks();
            student2.Marks = marks2;
            student2.Marks.Subject = "Programming";
            student2.Marks.TotalMarks = 100;
            student2.Attendance = new List<Models.Attendance>();
            student2.Attendance.Add(attendance3);
            _people.Add(student2);


            Models.Attendance attendance2 = new Attendance();
            attendance2.Date = DateTime.Parse("09/01/2020");
            attendance2.Present = true;
            attendance2.Date = DateTime.Parse("10/01/2020");
            attendance2.Present = false;

            Models.Student student3 = new Models.Student();
            student3.Id = 100;
            Marks marks3 = new Marks();
            student3.Marks = marks3;
            student3.Marks.Subject = "Programming";
            student3.Marks.TotalMarks = 100;
            student3.Attendance = new List<Models.Attendance>();
            student3.Attendance.Add(attendance2);
            _people.Add(student3);

        }
        public void Add(Person person)
        {
            _people.Add(person);
        }

        public List<Person> GetPeople()
        {
            return _people;
        }

        public Person GetPerson(int id)
        {
            return _people.FirstOrDefault(p => p.Id == id);
        }

        public List<Attendance> GetAttendances(int id)
        {
            return _people.Find(p => p.Id == id).Attendance;
        }

        public List<Person> ListPersons()
        {
            return _people;
        }
    }
}
