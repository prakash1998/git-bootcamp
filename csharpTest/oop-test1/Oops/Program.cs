﻿using System;
using System.Collections.Generic;
namespace Oops
{
    class Program
    {
        private static Services.PersonService _services = new Services.PersonService();

        static void Main(string[] args)
        {
            _services.AddTestDate();
            Console.WriteLine("select any one of the ids of students : \n");

            _services.ListPersons().ForEach(person => Console.WriteLine(person.Id));
            int id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("the attendance of the student is \n");

            _services.GetPerson(id).Attendance.ForEach(att => Console.WriteLine(att.ToString()));

            Console.WriteLine("\n the marks of the student are \n");
            Models.Student student = (Models.Student)_services.GetPerson(id);
            Console.WriteLine(student.Marks.Subject + " : " + student.Marks.TotalMarks);

            


        }




    }
}
