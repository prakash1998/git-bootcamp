﻿using System;

namespace Oops.Models
{
    public class Attendance
    {
        public DateTime Date { get; set; }

        public bool Present { get; set; }

        public Attendance(DateTime date,bool boolean)
        {
            Date = date;
            Present = boolean;
        }

        public Attendance()
        {

        }

        public override string ToString()
        {

            return Date.ToString() + Present.ToString(); 
        }
    }
}
