﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oops.Models
{
    /* marks can implement interface based on their course
     * for now it is just stores total marks */
    public class Marks
    {

        public string Subject { get; set; }
        public int TotalMarks { get; set; }
    }
}
